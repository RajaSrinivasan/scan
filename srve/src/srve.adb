with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;

with Ada.Command_Line ; use Ada.Command_Line;

with gnat.sockets ;
with gnat.Source_Info ;

procedure Srve is
   verbose : boolean := true ;
   procedure show_service( se : gnat.sockets.Service_Entry_Type ) is
   begin
      Put("Protocol "); Put(gnat.sockets.Protocol_Name(se));
      Put(" Name "); Put(gnat.sockets.Official_Name(se));
      Put(" port "); Put(Integer(gnat.sockets.Port_Number(se)));
      New_Line;
      for al in 1..gnat.sockets.Aliases_Length(se)
      loop
         Put("Alias "); Put(Integer(al)); Put(" "); Put(gnat.sockets.Aliases(se,al)); New_Line;
      end loop ;
   end show_service ;

   procedure show_service( p : String ; pn : String ) is
   begin
      declare
         se : gnat.sockets.Service_Entry_Type := gnat.sockets.Get_Service_by_Name(p,pn);
      begin
         show_service(se) ;
      end ;
   end show_service ;

   procedure show_service( p : gnat.sockets.Port_Type ; pn : String ) is
   begin
      declare
         se : gnat.sockets.Service_Entry_Type := gnat.sockets.Get_Service_by_Port(p,pn);
      begin
         show_service(se);
      end ;
     exception
         when GNAT.SOCKETS.SERVICE_ERROR =>
            Put("Protocol "); Put(pn) ; Put(" has no entry for port "); Put(Integer(p)); New_Line ;
            return;
   end show_service ;

   procedure t1 is
      myname : String := gnat.source_info.enclosing_entity ;
      pnum : gnat.sockets.Port_Type ;
   begin
      if verbose
      then
         Put_Line(myname);
      end if ;
      if argument_count < 2
      then
         Put_Line("usage: t1 portno");
         return ;
      end if ;
      pnum := gnat.sockets.port_type'Value( Argument(2) ) ;
      Put("Port Number "); Put(Integer(pnum)); New_Line ;
      show_service( pnum , "udp" ) ;
      show_service( pnum , "tcp" ) ;
   end t1 ;
   procedure t2 is
      myname : String := gnat.source_info.enclosing_entity ;
   begin
      if verbose
      then
         Put_Line(myname);
      end if ;
      if argument_count < 2
      then
         Put_Line("usage: t2 portname");
         return ;
      end if ;
      show_service( Argument(2) , "udp" ) ;
      show_service( Argument(2) , "tcp" ) ;
   exception
      when others =>
         Put("Protocol "); Put(Argument(2)) ; Put(" does not have an entry"); New_Line;
         return;
   end t2 ;
   procedure t3 is
      myname : String := gnat.source_info.enclosing_entity ;
      pfrom, pto : gnat.sockets.Port_Type ;
   begin
      if verbose
      then
         Put_Line(myname);
      end if ;

      if argument_count >= 2
      then
         pfrom := gnat.sockets.Port_Type'Value( Argument(2) );
      else
         pfrom := gnat.sockets.Port_Type'First ;
      end if ;
      if argument_count >= 3
      then
         pto := gnat.sockets.Port_Type'Value( Argument(3) );
      else
         pto := gnat.sockets.Port_Type'Last ;
      end if ;

      for p in pfrom..pto
      loop
         Put(Integer(p),base=>16); Put("-----------------"); New_Line;
         begin
            show_service(p,"udp");
            show_service(p,"tcp");
         exception
            when others =>
               Put(" does not have an entry"); New_Line;
         end ;
      end loop ;
   exception
      when others =>
         Put(" exception "); New_Line;
   end t3 ;

begin
   if Argument_Count < 1
   then
      Put_Line("usage: srve testname testargs");
      return ;
   end if ;

   if Argument(1) = "t1"
   then
      T1 ;
   elsif Argument(1) = "t2"
   then
      T2 ;
   elsif Argument(1) = "t3"
   then
      T3 ;
   end if ;

end Srve;
